/*
Create a trainer object using object literals.
	* Initialize/add the following trainer object properties:
		- Name (String)
	 	- Age (Number)
		- Pokemon (Array)
		- Friends (Object with Array values for properties)
	* Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
	* Access the trainer object properties using dot and square bracket notation.
	* Invoke/call the trainer talk object method.
	* Create a constructor for creating a pokemon with the following properties:
		- Name (Provided as an argument to the constructor)
		- Level (Provided as an argument to the constructor)
		- Health (Create an equation that uses the level property)
		- Attack (Create an equation that uses the level property)
	* Create/instantiate several pokemon objects from the constructor with varying name and level properties.
	* Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	* Create a faint method that will print out a message of targetPokemon has fainted.
	* Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
	* Invoke the tackle method of one pokemon object to see if it works as intended.
	
*/

const trainer = 
	{
		name: "Ash Ketchum",
		age: 10,
		pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
		friends:
			{
				Hoenn: ["May", "Max"],
 				Kanto: ["Brock", "Misty"]
			},
		talk: function()
				{
					console.log("Pikachu! I chose you!");
				}
	};
console.log(trainer);

console.log("Result of dot Notation:");
console.log(trainer.name);
console.log("Result of square bracket Notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method: ");
trainer.talk();



function Pokemon(name, level)
		{
			// Properties
			this.name = name;
			this.level = level;
			this.health = level * 2;
			this.attack = level;

			// this.Pokemon = [this.name, this.level, this.health, this.attack];
   //      	newList.push(this.Pokemon);


			// method       // target is object type
			this.tackle = function(target)
							{
								console.log(this.name + " tackled " + target.name);
								
								// newTargetPokemonHealth = targetPokemonHealth - pokemonAttack
								let newTargetHealth = target.health - this.attack;
								console.log(target.name +"'s health is reduced to " + newTargetHealth);
								target.health =	newTargetHealth

								// call faint method if the target pokemon's health is less that or equal to zero
								if (newTargetHealth <= 0)
									{
										this.faint(target);
									}
							}
			this.faint = function(target)
							{
								console.log(target.name + " fainted.");

							}
		}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

let mewtwo = new Pokemon("MewTwo", 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);